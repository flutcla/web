<h1>新歓イベント</h1>
<section>
<div class="calendar">
    <div class="April">
        <h2>2019年 4月</h2>
        <table>
            <thead><tr><td>月</td><td>火</td><td>水</td><td>木</td><td>金</td><td>土</td><td>日</td></tr></thead>
            <tbody id="calendar-body-april">
            </tbody>
        </table>
    </div>
    <div class="May">
        <h3>2019年 5月</h3>
        <table>
            <thead><tr><td>月</td><td>火</td><td>水</td><td>木</td><td>金</td><td>土</td><td>日</td></tr></thead>
            <tbody id="calendar-body-may">
            </tbody>
        </table>
    </div>
</div>
</section>
<section>
    <h2>イベント詳細</h2>
    <h3 id="kobetsu">個別オリエンテーション</h3>
    <p>個別オリエンテーションでは、サイテクの活動についてスライドを用いて説明します。</p>
    <h3 id="zentai">全体オリエンテーション</h3>
    <p>全体オリエンテーションとは、園遊会が開催される日に講堂にて行われる複数サークル合同のオリエンテーションです。</p>
    <p>サイテクでは、活動に関する説明と、科学に関するショーを行います。</p>
    <h3 id="eventa">新歓イベント『の』: サイテク式知恵の輪 (4/17) ※参加申し込みが必要</h3>
    <p>サイテク式知恵の輪は"再帰"をテーマとした工作を行います。</p>
    <p>大学で学ぶ学問の上でも重要な概念ですので、ぜひ工作を通して学びましょう。</p>
    <h3 id="eventb">新歓イベント『虹』: 虹色スコープ (4/20) ※参加申し込みが必要</h3>
    <p>虹色スコープは"分光"をテーマとした工作を行います。</p>
    <p>白色光を分光するとどうなるか、知識と経験を結びつける体験をしましょう。</p>
    <br>
    <p>上記新歓イベントへの参加をご希望の方は、<a href="../contact/#eventform" style="color:blue; font-weight:bold;">こちら</a>から申し込みをお願いします。</p>
</section>
