<section>
    <ul class="bxslider">
        <li><img src="images/top/top1.jpg" alt=""></li>
        <li><img src="images/top/top2.jpg" alt=""></li>
        <li><img src="images/top/top3.jpg" alt=""></li>
    </ul>
</section>
<section>
    <h2>ようこそ！</h2>
    <p>皆さん、東工大ScienceTechno(通称:サイテク)の新歓HPへようこそお越し下さいました。</p>
    <p>サイテクは、「子どもと科学を楽しもう」をモットーに活動を行っているサークルです。</p>
    <p>本ホームページには、サイテクの活動の紹介や、新歓活動の日程、部室へのアクセス等のコンテンツがあります。わからない点などがありましたら、お問い合わせフォームや、Twitterアカウント等に、気軽にご連絡をよろしくお願いします。</p>
</section>
<section>
    <h2>入部をご希望の方へ</h2>
    <p><strong>入部したい場合は4/24(水)の昼休みにW621で行われる入部会への参加をよろしくお願いします。</strong></p>
    <p>もし、参加が難しい場合はお問い合わせフォームよりご連絡お願いします。</p>
</section>
<section class="split">
<section>
    <h2>Twitter<a href="https://twitter.com/shinkan_scitech" style="font-size: 12px; color: inherit;"> (@shinkan_scitech)</a></h2>
    <a class="twitter-timeline" data-height="800" href="https://twitter.com/shinkan_scitech?ref_src=twsrc%5Etfw">Tweets by shinkan_scitech</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</section>
<section>
    <h2>Instagram <a href="https://www.instagram.com/shinkan_scitech/" style="font-size: 12px; color: inherit;"> (@shinkan_scitech)</a></h2>
    <a href="https://instawidget.net/v/user/shinkan_scitech" id="link-6371fe84406542d025fe21f2691ff575ea0565acb6249ad40461065a8a23ca5b">@shinkan_scitech</a>
    <script src="https://instawidget.net/js/instawidget.js?u=6371fe84406542d025fe21f2691ff575ea0565acb6249ad40461065a8a23ca5b&width=100%"></script>
</section>
</section>
