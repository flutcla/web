<h1>お問い合わせ・イベント申し込み</h1>
<div class="cp_tab">
	<input type="radio" name="cp_tab" id="tab1_1" aria-controls="first_tab01" checked>
	<label for="tab1_1">お問い合わせ</label>
	<input type="radio" name="cp_tab" id="tab1_2" aria-controls="second_tab01">
	<label for="tab1_2">イベント申し込み</label>
	<div class="cp_tabpanels">
		<div id="first_tab01" class="cp_tabpanel">
			<section>
				<h2>お問い合わせ</h2>
				<p>お問い合わせは以下のフォーム、又は<a href="https://twitter.com/shinkan_scitech">Twitter(@shinkan_scitech)</a>までよろしくお願いします。</p>
				<form action="https://docs.google.com/forms/d/e/1FAIpQLSfXwY0tnq2FU9pvfoEWUPG4wTuAuFEu7fewvb951bJZLT0K-g/formResponse" method="POST">
					<label for="name">名前</label>
					<input type="text" name="entry.2005620554" placeholder="歳手句　太郎" required>
					<label for="major">所属学院</label>
					<select name="entry.1309051012" required>
						<option value="その他">その他</option>
						<option value="理学院">理学院</option>
						<option value="工学院">工学院</option>
						<option value="物質理工学院">物質理工学院</option>
						<option value="情報理工学院">情報理工学院</option>
						<option value="生命理工学院">生命理工学院</option>
						<option value="環境・社会理工学院">環境・社会理工学院</option>
					</select>
					<label for="email">メールアドレス</label>
					<input id="email" type="text" name="entry.1045781291" placeholder="example@mail.co.jp" required>
					<label for="msg">お問い合わせ内容</label>
					<textarea name="entry.839337160" placeholder="お問い合わせ内容を入力してください。" required></textarea>
					<button type="submit" name="button" value="送信">送信</button>
				</form>
			</section>
		</div>
		<div id="second_tab01" class="cp_tabpanel">
			<section>
				<h2>新歓イベント 参加申込</h2>
				<p>新歓イベントの参加申込は以下のフォームを利用してください。</p>
				<form action="https://docs.google.com/forms/d/e/1FAIpQLSeTYD74tSBdbIxQuIn7cLoFon7pqCEkXDjjZZHT5HOlWRXFIA/formResponse" method="POST">
					<label for="name">名前</label>
					<input type="text" name="entry.1861222181" placeholder="歳手句　太郎" required>
					<label for="name2">ふりがな</label>
					<input type="text" name="entry.137493768" placeholder="さいてく　たろう" required>
					<label for="email">メールアドレス</label>
					<input type="text" name="entry.2094012224" placeholder="example@mail.co.jp" required>
					<label for="grade">学年</label>
					<select name="entry.1589137618" required>
						<option value="1年生">1年生</option>
						<option value="2年生">2年生</option>
						<option value="3年生">3年生</option>
					</select>
					<label for="major">所属学院</label>
					<select name="entry.826065995" required>
						<option value="その他">その他</option>
						<option value="理学院">理学院</option>
						<option value="工学院">工学院</option>
						<option value="物質理工学院">物質理工学院</option>
						<option value="情報理工学院">情報理工学院</option>
						<option value="生命理工学院">生命理工学院</option>
						<option value="環境・社会理工学院">環境・社会理工学院</option>
					</select>
					<label for="event">参加希望イベント</label>
					<label for="checkbox1"><input disabled type="checkbox" id="checkbox1" name="entry.51658721" value="4/17(水)「の」 ～サイテク式知恵の輪～"><s>4/17(水)「サイテク式知恵の輪」</s><b>終了しました</b></label>
					<label for="checkbox2"><input type="checkbox" id="checkbox2" name="entry.51658721" value="4/20(土)「虹」 ～虹色スコープ～">4/20(土)「虹色スコープ」</label>
					<label for="msg">その他</label>
					<textarea id="msg" name="entry.1887565241" placeholder="質問等ありましたらこちらへご記入ください。"></textarea>
					<button type="submit" name="button" value="送信">送信</button>
				</form>
			</section>
		</div>
	</div>
</div>
