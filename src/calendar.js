$(function(){
    var april = '<tr>';
    var today = new Date()
    for(var i=1; i<=28; i++){
        if((today.getDate()==i)&&(today.getMonth()==3)){
            april += `<td class="today"><h6>${i}</h6></td>`;
        }else{
            april += `<td><h6>${i}</h6></td>`;
        }
        if(i%7 == 0){
            april += '</tr><tr>';
        }
    }
    april += '</tr>';
    $('#calendar-body-april').html(april);
});


$(function(){
    var events = {'0401': [['kobetsu','@S224<br>10:00~12:00<br>13:00~15:00'], ['kenkou']], '0402': [['nyugaku']],
        '0403': [['kobetsu','@H121<br>10:00~12:00<br>13:00~15:00'], ['kenkou']], '0408': [['kobetsu','@W242<br>7限~10限']], '0409': [['kobetsu','@W321<br>9,10限']],
        '0410': [['show','@W521<br>14:00~14:30']], '0411': [['kobetsu','@W242<br>7~10限']], '0412': [['kobetsu','@W332<br>9,10限']],
        '0413': [['zentai','@70周年記念講堂'], ['enyukai']], '0415': [['kobetsu','@W323<br>9,10限']], '0417': [['shinkana','@H137<br>14:00~15:30<br>本館前に13:45集合']],
        '0420': [['shinkanb','@H102 10:30~12:00<br>本館前に10:15集合']], '0423': [['kobetsu','@W631<br>9,10限']], '0424': [['nyubukai','昼休み']],
        '0427': [['nyubuori','@S221~S224<br>13:00~17:30']]};
    var event_name = {'kobetsu': '個別オリ', 'kenkou': '健康診断', 'nyugaku': '入学式',
        'show': '新歓ショー', 'enyukai': '園遊会', 'shinkana': '新歓イベ の', 'shinkanb': '新歓イベ 虹',
        'zentai': '全体オリ', 'nyubukai': '入部会', 'nyubuori': '入部オリ'}
    for(var key of Object.keys(events)){
        month = key.slice(0, 2)/1;
        date = key.slice(2)/1;
        for(var cls of events[key]){
            if(month==4){
                $('#calendar-body-april').find('td').eq(date-1).append(`<a href="#${cls[0]}"><div class="event ${cls[0]}"><p>${event_name[cls[0]]}</p><p class="where">${cls[1]||''}</p></div></a>`);
            }else if(month==5){
                $('#calendar-body-may').find('td').eq(date-1).append(`<a href="#${cls[0]}"><p class="event ${cls[0]}">${event_name[cls[0]]}</p></a>`);
            }
        }
    }
});
