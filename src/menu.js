$(document).ready(function() {
	$('.menu-box').click(function () {
	  $(this).toggleClass('active');
	  $('.menu-list').toggleClass('onanimation');
	});
});

$(function(){
    var flag = true;
    $('.menu-right ul li a').each(function(){
        var $href = $(this).attr('href')
        if(location.href.match($href)) {
            $(this).addClass('current');
            flag = !flag;
        } else {
            $(this).removeClass('current');
        }
    });
    if(flag){
        $('.current:first').removeClass('current');
    };
});
