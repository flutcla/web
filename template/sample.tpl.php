<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--タイトルやTwitterカードなど-->
    <meta name="description" content="東工大ScienceTechno(サイテク)新歓用特設サイトです。サイテクは、主に小学生を対象に、ショーや工作を含む科学教室を企画・運営する東工大公認サークルです。">
    <title> <?php echo $v->title; ?> | 東工大ScienceTechno新歓2019</title>
    <link rel="shortcut icon" href="..\images\favicon.ico">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@shinkan_scitech">
    <meta property="og:url" content="----------------------------------------">
    <meta property="og:title" content="<?php echo $v->title; ?> | 東工大ScienceTechno">
    <meta property="og:description" content="東工大ScienceTechno(サイテク)新歓用特設サイトです。サイテクは、主に小学生を対象に、ショーや工作を含む科学教室を企画・運営する東工大公認サークルです。">
    <meta property="og:image" content="../images/sciencetechno.png">
    <!--CSS,JS-->
    <link rel="stylesheet" type="text/css" href="../import.css">
    <script type="text/javascript" src="../src/jQuery.js"></script>
    <script type="text/javascript" src="../src/menu.js"></script>
    <script type="text/javascript" src="../src/calendar.js"></script>
    <script type="text/javascript" src="../src/bxslider.js"></script>
    <script type="text/javascript" src="../src/twitter.js"></script>
    <script type="text/javascript" src="../src/event.js"></script>
    <script type="text/javascript">
    function changeTwitterWidgetDesign(){
      var $twitter_widget = $('iframe.twitter-timeline');
      var $twitter_widget_contents = $twitter_widget.contents();
      if ($twitter_widget.length > 0 && $twitter_widget[0].contentWindow.document.body.innerHTML !== ""){
        $twitter_widget_contents.find('head').append('<link href="../src/twitter.css" rel="stylesheet" type="text/css">');
      }
      else {
        setTimeout(function(){
          changeTwitterWidgetDesign();
        }, 350);
      }
    }
    changeTwitterWidgetDesign();
    </script>
</head>

<body>
    <!--メニュー-->
    <div class="header-sp">
        <a href="/"><img src="../images/sciencetechno.png"></a>
        <input type="checkbox" id="menu">
        <label for="menu" class="menu-icon">
            <span></span>
            <span>MENU</span>
            <span></span>
        </label>
        <div class="menu-list">
            <ul>
                <li><a href="/">トップページ</a></li>
                <li><a href="/about">サイテクとは</a></li>
                <li><a href="/activity">活動内容</a></li>
                <li><a href="/event">新歓イベント</a></li>
                <li><a href="/access">アクセス</a></li>
                <li><a href="/contact">お問い合わせ</a></li>
            </ul>
        </div>
        </div>
    </div>

    <div class="menu-p">
        <div class="menu-left">
            <a href="/"><img src="../images/sciencetechno.png"></a>
        </div>
        <div class="menu-right">
            <ul>
                <li><a href="/"><span>トップ</span><br>top</a></li>
                <li><a href="/about"><span>サイテクとは</span><br>about</a></li>
                <li><a href="/activity"><span>活動内容</span><br>activity</a></li>
                <li><a href="/event"><span>新歓イベント</span><br>event</a></li>
                <li><a href="/access"><span>アクセス</span><br>access</a></li>
                <li><a href="/contact"><span>お問い合わせ</span><br>contact</a></li>
            </ul>
        </div>
    </div>
    <!--コンテンツ-->
    <div class="container">
        <?php echo $v->content; ?>
    </div>
    <div class="footer">
        <p>&copy; 東工大ScienceTechno 2019</p>
    </div>
</body>

</html>
