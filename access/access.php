<h1>アクセス</h1>
<section>
<h2>概略図</h2>
<img src="../images/maps/map.jpg" alt="概略図">
</section>
<section>
<h2>写真付き案内</h2>
<h3>1. 正門から図書館に向かって奥の道を右に曲がる</h2>
<img src="../images/maps/1.jpg" alt="写真1">
<h3>2. 工事現場の手前を左に曲がり、建物まで直進する</h2>
<img src="../images/maps/2.jpg" alt="写真2">
<h3>3. 突きあたりのドアを入って左に進む</h2>
<img src="../images/maps/3.jpg" alt="写真3">
<h3>4. 手前から２番目の部屋がサイテクの部室です！</h2>
<img src="../images/maps/4.jpg" alt="写真4">
</section>
